class CfgAILevelPresets {
  class AILevelLow {
    displayName = "SFD";
    precisionAI = 0.255;
    skillAI = 0.98;
  };
};
